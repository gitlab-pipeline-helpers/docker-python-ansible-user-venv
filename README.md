# Docker image with Ansible and Galaxy modules installed

blog: https://fabianlee.org/2023/10/14/gitlab-invoking-ansible-from-a-gitlab-pipeline-job/

Have purposely not locked down version of Ansible or requirements.txt so that next image build gets newer set of dependencies.  But copied [freeze.txt](freeze.txt), as historical record of exact pip modules.

### Pulling image

```
registry.gitlab.com/gitlab-pipeline-helpers/docker-python-ansible-user-venv:latest
```

### Creating tag that invokes Gitlab Pipeline

```
newtag=v1.0.0
git commit -a -m "changes for new tag $newtag" && git push -o ci.skip && git tag $newtag && git push origin $newtag
```

### Deleting tag

```
# delete local tag, then remote
todel=v1.0.0
git tag -d $todel && git push --delete origin $todel
